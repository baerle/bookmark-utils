const URI_MATCH = new RegExp("http[s]?:\/{2}(www\.)?([\/\\w-\\.]*)", "i");

class Bookmark {
  title;
  url;
  path;

  constructor (id, title, url, path) {
    this.id = id;
    this.title = title;
    this.url = url;
    this.path = path;
  }
}

function addDuplicates(duplicates) {
  const tbody = document.getElementsByTagName("tbody")[0];
  const template = document.getElementById("tbodyTemplate");
  duplicates.forEach((duplicate) => {
    //console.log(duplicate);
    template.content.querySelector(".check1").dataset.id = duplicate[0].id;
    template.content.querySelector(".url1").textContent = duplicate[0].url;
    template.content.querySelector(".title1").textContent = duplicate[0].title ? duplicate[0].title : "";
    template.content.querySelector(".path1").textContent = duplicate[0].path ? duplicate[0].path : "";

    template.content.querySelector(".check2").dataset.id = duplicate[1].id;
    template.content.querySelector(".url2").textContent = duplicate[1].url;
    template.content.querySelector(".title2").textContent = duplicate[1].title ? duplicate[1].title : "";
    template.content.querySelector(".path2").textContent = duplicate[1].path ? duplicate[1].path : "";
    const clone = document.importNode(template.content, true);
    tbody.appendChild(clone);
  });
}

getBookmarks(setBookmarks);
document.getElementById("deleteSelected").addEventListener("click", () => {
  let toDelete = new Array();
  for (const tr of document.getElementsByTagName("tr")) {
    let input = tr.children[0].children[0];
    if (input && input.checked) {
      //console.log("id: " + input.dataset.id);
      toDelete.push(input.dataset.id);
    }
    input = tr.children[2].children[0];
    if (input && input.checked) {
      //console.log("id: " + input.dataset.id);
      toDelete.push(input.dataset.id);
    }
  }
  console.log(toDelete);

  toDelete.forEach(b => deleteBookmark(b));
});

function deleteBookmark(id) {
  chrome.bookmarks.remove(id, console.log);
}

function getBookmarks(callback) {
  chrome.bookmarks.getTree(callback);
}

function setBookmarks(bookmarkItems) {
  bookmarkRoot = bookmarkItems[0];
  addDuplicates(getIndexesOfDuplicates(getAllBookmarks(bookmarkRoot)));
}

function getAllBookmarks(bookmarkRoot) {
  let allBookmarks = new Set();

  function addBookmark(element, parents) {
    if (element) {
      if (element.url) {
        try {
          allBookmarks.add(new Bookmark(element.id, element.title, element.url, parents));
        } catch (e) {
          console.log(e);
        }
      } else if (element.children) {
        for (child of element.children) {
          addBookmark(child, parents + '\\' + element.title);
        }
      }
    }
  }

  addBookmark(bookmarkRoot);

  console.log(allBookmarks);

  return allBookmarks;
}

function sortBookmarksAfterURL(bookmarks) {
  return Array.from(bookmarks).sort((a, b) => {
    const x = a.url.match(URI_MATCH) ? a.url.match(URI_MATCH)[2] : null;
    const y = b.url.match(URI_MATCH) ? b.url.match(URI_MATCH)[2] : null;
    return x == y ? 0 : x > y ? 1 : -1;
  });
}

function getIndexesOfDuplicates(allBookmarks) {
  let duplicates = [];
  let bookmarks = sortBookmarksAfterURL(allBookmarks);
  console.log(bookmarks);

  for (let i = 0; i < bookmarks.length; i++) {
    if (isEqualMainURI(i)) {
      duplicates.push([bookmarks[i], bookmarks[i + 1]]);
    }
  }

  console.log(duplicates);

  return duplicates;

  function isEqualMainURI(i) {
    if (bookmarks[i + 1]) {
      const bookmarkMatch = bookmarks[i].url.match(URI_MATCH);
      const bookmarkMatch1 = bookmarks[i + 1].url.match(URI_MATCH);
      if (bookmarkMatch && bookmarkMatch1) {
        return bookmarkMatch[2] == bookmarkMatch1[2];
      }
    }
    return false;
  }
}

function findDoubledBookmarks(bookmarkRoot) {
  getBookmarks(bookmarkRoot);
  console.log(bookmarkRoot);
  //   logTree(bookmarks);
  if (bookmarkRoot != undefined) {
    const allBookmarks = getAllBookmarks();
    const duplicates = getIndexesOfDuplicates(allBookmarks);
    return duplicates;
  }
}

function makeIndent(indentLength) {
  return ".".repeat(indentLength);
}

function logItems(bookmarkItem, indent) {
  if (bookmarkItem.url) {
    console.log(makeIndent(indent) + bookmarkItem.url);
  } else {
    console.log(makeIndent(indent) + "Folder");
    indent++;
  }
  if (bookmarkItem.children) {
    for (child of bookmarkItem.children) {
      logItems(child, indent);
    }
  }
  indent--;
}

function logTree(bookmarkItems) {
  logItems(bookmarkItems.url, 0);
}

function logSet(bookmarks) {
  for (const bm of bookmarks) {
    console.log(bm.url);
  }
}
