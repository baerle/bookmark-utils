// import {logTree} from 'logBookmarks.js';

const URI_MATCH = new RegExp("http[s]?:\/{2}(w\.)?(.*\.[a-z]+[\/\w\d-]*)", "i");
var bookmarkRoot;

function onRejected(error) {
    console.log(`An error: ${error}`);
}

function getBookmarks(bookmarks) {
    const gettingTree = browser.bookmarks.getTree();
    gettingTree.then(setBookmarks, onRejected);
}

function setBookmarks(bookmarkItems) {
    bookmarkRoot = bookmarkItems[0];
}

function getAllBookmarks() {
    function addBookmark(element, parents) {
        if (element) {
            if (element.url) {
                try {
                    allBookmarks.add([element.url, element.id,]);
                } catch (e) {
                    console.log(e);
                }
            } else if (element.children) {
                for (child of element.children) {
                    addBookmark(child, parents + element.title);
                }
            }
        }
    }

    let allBookmarks = new Set();

    addBookmark(bookmarkRoot);

    console.log(allBookmarks);

    return allBookmarks;
}

function getIndexesOfDuplicates(allBookmarks) {
    let duplicates = [];
    let bookmarks = Array.from(allBookmarks).sort((a, b) => {
        const x = a[0].match(URI_MATCH) ? a[0].match(URI_MATCH)[2] : null;
        const y = b[0].match(URI_MATCH) ? b[0].match(URI_MATCH)[2] : null;
        return x == y ? 0 : x > y ? 1 : -1;
    });
    console.log(bookmarks);

    for (let i = 0; i < bookmarks.length; i++) {
        if (isEqualMainURI(i)) {
            duplicates.push([bookmarks[i], bookmarks[i + 1]]);
            console.log("found duplicate: " + bookmarks[i] + " - " + bookmarks[i + 1]);
        }
    }

    console.log(duplicates);

    return duplicates;

    function isEqualMainURI(i) {
        if (bookmarks[i + 1]) {
            const bookmarkMatch = bookmarks[i][0].match(URI_MATCH);
            const bookmarkMatch1 = bookmarks[i + 1][0].match(URI_MATCH);
            if (bookmarkMatch && bookmarkMatch1) {
                return bookmarkMatch[2] == bookmarkMatch1[2];
            }
        }
        return false;
    }
}

function findDoubledBookmarks() {
    getBookmarks(bookmarkRoot);
    console.log(bookmarkRoot);
    //   logTree(bookmarks);
    if (bookmarkRoot != undefined) {
        const allBookmarks = getAllBookmarks();
        const duplicates = getIndexesOfDuplicates(allBookmarks);
        return duplicates;
    }
}
